import { NgModule } from '@angular/core';
import { AbButtonComponent } from './ab-button.component';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
    imports: [
        BrowserModule
    ],
    declarations: [
        AbButtonComponent
    ],
    exports: [
        AbButtonComponent
    ]
})
export class AbButtonModule {

}
