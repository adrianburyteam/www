import { Component, OnInit, ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import { SidebarLinkService } from './shared/sidebar-link/shared/sidebar-link.service';
import { SidebarLinkModel } from './shared/sidebar-link/shared/sidebar-link.model';

@Component({
    selector: 'my-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.sass'],
    providers: [
        SidebarLinkService
    ]
})
export class SidebarComponent implements OnInit, AfterViewInit {
    @ViewChild('burgerEl') burger: ElementRef;
    @ViewChild('overflowEl') overflow: ElementRef;
    @ViewChild('navEl') nav: ElementRef;

    pageNavClassList;
    pageNavShowClassName = 'page-nav-show';
    pageNavOverflowClassList;
    pageNavOverflowClassName = 'show';
    links: SidebarLinkModel[];

    constructor(private sidebarLinkService: SidebarLinkService) {
    }

    togglePageNav() {
        if (this.pageNavClassList.contains(this.pageNavShowClassName)) {
            this.pageNavClassList.remove(this.pageNavShowClassName);
            this.pageNavOverflowClassList.remove(this.pageNavOverflowClassName);
        } else {
            this.pageNavClassList.add(this.pageNavShowClassName);
            this.pageNavOverflowClassList.add(this.pageNavOverflowClassName);
        }
    }

    ngOnInit() {
        this.getSidebarLinks();
    }

    ngAfterViewInit(): void {
        this.pageNavClassList = this.nav.nativeElement.classList;
        this.pageNavOverflowClassList = this.overflow.nativeElement.classList;
    }

    private getSidebarLinks() {
        this.sidebarLinkService.getLinks().subscribe(
            (sidebarLink) => this.links = sidebarLink
        );
    }

}
