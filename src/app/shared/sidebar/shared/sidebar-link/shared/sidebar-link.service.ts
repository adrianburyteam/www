import { Injectable } from '@angular/core';
import { SIDEBAR_LINKS } from './sidebar-link.mock';
import { Observable } from 'rxjs/Observable';
import { SidebarLinkModel } from './sidebar-link.model';


@Injectable()
export class SidebarLinkService {
    public getLinks(): Observable<SidebarLinkModel[]> {
        return Observable.of(SIDEBAR_LINKS);
    }

}
