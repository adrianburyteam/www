import { SidebarLinkModel } from './sidebar-link.model';

export const SIDEBAR_LINKS: SidebarLinkModel[] = [
    {
        text: 'Home',
        className: 'fa fa-home',
        link: ''
    }, {
        text: 'O mnie',
        className: 'fa fa-id-card',
        link: 'about'
    }, {
        text: 'Oferta',
        className: 'fa fa-coffee',
        link: 'offer'
    }, {
        text: 'Projekty',
        className: 'fa fa-code',
        link: 'projects'
    }, {
        text: 'Referencje',
        className: 'fa fa-users',
        link: 'recommends'
    }, {
        text: 'Kontakt',
        className: 'fa fa-comments-o',
        link: 'contact'
    }
];
