export class SidebarLinkModel {
    text: String;
    link: String;
    className: String;

    constructor() {
        this.text = '';
        this.link = '';
        this.className = '';
    }


}
