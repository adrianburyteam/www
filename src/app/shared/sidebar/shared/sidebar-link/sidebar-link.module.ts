import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';


import { SidebarLinkComponent } from './sidebar-link.component';


@NgModule({
    imports: [
        RouterModule,
    ],
    declarations: [
        SidebarLinkComponent,
    ],
    exports: [
        SidebarLinkComponent
    ]
})
export class SidebarLinkModule {

}
