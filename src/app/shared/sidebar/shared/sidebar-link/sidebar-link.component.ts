import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'my-sidebar-link',
    templateUrl: 'sidebar-link.component.html',
    styleUrls: ['sidebar-link.component.sass']
})
export class SidebarLinkComponent implements OnInit {
    @Input() link;

    constructor() {
        // Do stuff
    }

    ngOnInit() {
    }

}
