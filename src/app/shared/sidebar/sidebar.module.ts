import { NgModule } from '@angular/core';

import { SidebarComponent } from './sidebar.component';
import { SidebarLinkModule } from './shared/sidebar-link/sidebar-link.module';
import { PERFECT_SCROLLBAR_CONFIG, PerfectScrollbarModule } from 'ngx-perfect-scrollbar';


@NgModule({
    imports: [
        PerfectScrollbarModule.forRoot(PERFECT_SCROLLBAR_CONFIG),
        SidebarLinkModule
    ],
    declarations: [
        SidebarComponent
    ],
    exports: [
        SidebarComponent
    ]
})
export class SidebarModule {

}
