import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Headers } from '@angular/http';
import { Http, RequestOptions } from '@angular/http';

@Injectable()
export class ApiService {
    private apiUrl: String = 'http://api.adrianbury.pl/portfolio';

    constructor(private http: Http) {
    };

    public get(url: string): Observable<any> {
        return this.http.get(this.apiUrl + url)
            .catch(this.handleError)
            .map(this.extractData);
    }

    public post(url: string, body: any): Observable<any> {
        let postHeaders: Headers = new Headers({'Content-Type': 'application/json'});
        let options = new RequestOptions({
            headers: postHeaders
        });

        return this.http.post(this.apiUrl + url, body, options)
            .catch(this.handleError)
            .map(this.extractData);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || {};
    }

    private handleError(error: Response) {
        console.log('ERROR');
        console.log(error);
        return Observable.throw(error);
    }


}
