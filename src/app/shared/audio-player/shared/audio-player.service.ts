import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AudioPlayerModel } from './audioList.model';
import { ApiService } from '../../api.service';

@Injectable()
export class AudioPlayerService {

    constructor(private apiService: ApiService) {
    }

    getAudioList(): Observable<AudioPlayerModel[]> {
        return this.apiService.get('/music_corner/');
    }

}
