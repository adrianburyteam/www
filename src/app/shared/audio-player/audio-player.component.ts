import { Component, OnInit } from '@angular/core';
import { AudioPlayerService } from './shared/audio-player.service';
import { AudioPlayerModel } from './shared/audioList.model';

@Component({
    selector: 'my-audio-player',
    templateUrl: 'audio-player.component.html',
    styleUrls: [
        'audio-player.component.sass'
    ],
    providers: [
        AudioPlayerService
    ]
})
export class AudioPlayerComponent implements OnInit {
    private audio;
    private audioList: AudioPlayerModel[];
    private current: number = -1;
    private size: number;
    public isPlay = false;

    constructor(private audioPlayerService: AudioPlayerService) {
    }

    ngOnInit() {
        this.audio = document.createElement('audio');
        this.audio.setAttribute('loop', '');
        this.getAudioList();
    }

    play() {
        if (!this.audio.hasAttribute('src')) {
            this.next();
            return;
        }
        this.audio.play();
        this.isPlay = true;
    }

    stop() {
        this.audio.pause();
        this.isPlay = false;
    }

    getAudioList() {
        this.audioPlayerService.getAudioList()
            .subscribe(
                response => {
                    this.audioList = response;
                    this.size = this.audioList.length;
                }
            );
    }

    next() {
        this.current++;
        if (this.current >= this.size) {
            this.current = 0;
        }
        let url = this.audioList[this.current].music;
        this.audio.setAttribute('src', url);
        this.audio.load();
        this.play();
    }
}
