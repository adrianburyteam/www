import { NgModule } from '@angular/core';

import { AudioPlayerComponent } from './audio-player.component';
import { BrowserModule } from '@angular/platform-browser';

@NgModule({
    imports: [
        BrowserModule
    ],
    exports: [
        AudioPlayerComponent
    ],
    declarations: [AudioPlayerComponent],
    providers: [],
})
export class AudioPlayerModule {
}
