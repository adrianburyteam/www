import { NgModule } from '@angular/core';

import { HomeComponent }   from './home.component';
import { RouterModule } from '@angular/router';
import { AbButtonModule } from '../shared/ab-button/ab-button.module';

@NgModule({
    imports: [
        RouterModule,
        AbButtonModule
    ],
    exports: [],
    declarations: [HomeComponent],
    providers: [],
})
export class HomeModule {
}
